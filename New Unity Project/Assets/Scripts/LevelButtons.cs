﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelButtons : MonoBehaviour {
	
	protected Scene currentScene;
	public Button levelOneButton;
	public Button levelTwoButton;
	public Button levelThreeButton;

	// Use this for initialization
	void Start () {

		levelOneButton.onClick.AddListener (LevelOneButtonClicked);
		levelTwoButton.onClick.AddListener (LevelTwoButtonClicked);
		levelThreeButton.onClick.AddListener (LevelThreeButtonClicked);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void LevelOneButtonClicked()
	{
		Initialize ();
		SceneManager.LoadScene (currentScene.buildIndex + 1);
	}	

	void LevelTwoButtonClicked()
	{
		Initialize ();
		SceneManager.LoadScene (currentScene.buildIndex + 2);
	}	

	void LevelThreeButtonClicked()
	{
		Initialize ();
		SceneManager.LoadScene (currentScene.buildIndex + 3);
	}

	void Initialize ()
	{
		currentScene = SceneManager.GetActiveScene ();
	}
}
