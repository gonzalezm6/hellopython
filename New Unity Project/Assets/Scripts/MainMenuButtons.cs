﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuButtons : MonoBehaviour {

	protected Scene currentScene;
	public Button startButton;

	// Use this for initialization
	// Detects if button is clicked
	void Start () {

		startButton.onClick.AddListener (StartButtonClicked);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// If button is clicked, advance to "Level" scene
	void StartButtonClicked()
	{
		Initialize ();
		SceneManager.LoadScene (currentScene.buildIndex + 1);
	}

	// Define what the current scene is
	void Initialize ()
	{
		currentScene = SceneManager.GetActiveScene ();
	}
}
