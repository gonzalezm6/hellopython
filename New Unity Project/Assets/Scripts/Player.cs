﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class Player : MonoBehaviour {

	protected Scene currentScene;


	void Start () 
	{
		
	}

	// Constantly checks the "lives" count and
	// changes scene to "Game Over" when player
	// runs out of lives, reset lives count,
	// and makes mouse visible
	void Update () 
	{
		if (Global.lives == 0) 
		{
			Initialize ();
			SceneManager.LoadScene (currentScene.buildIndex + 1);
			Global.lives = 3;
			Cursor.visible = true; 

		}
	}

	// Define what the current scene is
	void Initialize ()
	{
		currentScene = SceneManager.GetActiveScene ();
	}
			
}
