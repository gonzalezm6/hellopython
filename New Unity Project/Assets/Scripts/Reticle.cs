﻿using UnityEngine;

public class Reticle : MonoBehaviour {

	public float rayCastDistance = 100.0f; 

	// Use this for initialization
	void Start () {
	
		Cursor.visible = false; 

	}
	
	// Update is called once per frame
	void Update () {


		// This defines the ray that is cast
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition); 
		// This defines the raycast hit varible; 
		RaycastHit hit;
		// Temporary position vector
		Vector3 tempVec = transform.position;
		// Casts the ray, stores the ht information into the hit variable 
		if(Physics.Raycast(ray, out hit, rayCastDistance))
		{
			//print("The ray hit something"); 
			tempVec.x = hit.point.x;
			transform.position = tempVec; 
		}
	}
}
