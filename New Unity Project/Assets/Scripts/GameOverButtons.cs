﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverButtons : MonoBehaviour {

	protected Scene currentScene;
	public Button menuButton;

	// Use this for initialization
	// Detects if button is clicked
	void Start () {

		menuButton.onClick.AddListener (MenuButtonClicked);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// If button is clicked, go back to "Main Menu" scene
	void MenuButtonClicked()
	{
		Initialize ();
		SceneManager.LoadScene (currentScene.buildIndex - 2);
	}
		
	// Define what the current scene is
	void Initialize ()
	{
		currentScene = SceneManager.GetActiveScene ();
	}
}
