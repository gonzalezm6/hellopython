﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global : MonoBehaviour {

	public static bool WinScreen = false;
	public static int lives = 3;

	// Function that provides an equation as to how
	// many lives are lost
	public static void LoseLives (int livesDecrement)
	{
		if (lives > 0) 
		{
			lives -= livesDecrement;
		}
	}
		
}
