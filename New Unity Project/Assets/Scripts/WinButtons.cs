﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinButtons : MonoBehaviour {

	protected Scene currentScene;
	public Button menuButton;

	// Use this for initialization
	// Detects if button is clicked
	void Start () {

		menuButton.onClick.AddListener (MenuButtonClicked);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// If button is clicked, go back to "Main Menu" scene
	void MenuButtonClicked()
	{
		print ("Button clicked");
		Initialize ();
		SceneManager.LoadScene (currentScene.buildIndex - 3);
	}

	// Define what the current scene is
	void Initialize ()
	{
		currentScene = SceneManager.GetActiveScene ();
	}
}
