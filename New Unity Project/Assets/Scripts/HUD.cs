﻿using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

	public Text livesText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	// Displays the lives count text
	void Update () {

		livesText.text = "Lives: " + Global.lives;
		
	}
			
}
