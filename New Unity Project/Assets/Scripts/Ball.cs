﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour {

	public bool playerHit = true;
	public GameObject respawnPoint;
	public GameObject newRespawnPoint;
	public GameObject newBall;
	protected Scene currentScene;


	// Use this for initialization
	void Start () {
				
		gameObject.GetComponent<Rigidbody> ().AddForce (gameObject.transform.right * 1500f);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// Used to detect when the ball collides with an object
	void OnCollisionEnter (Collision col)
	{
		switch (col.gameObject.tag) {
		case "Box":
			Destroy (col.gameObject);
			break;
		case "ExtraBall":
			print ("ExtraBall");
			Instantiate (newBall, newRespawnPoint.transform.position, newRespawnPoint.transform.rotation);
			Destroy (col.gameObject);
			break;
		case "ResetLives":
			print ("ResetLives");
			Global.lives = 3;
			Destroy (col.gameObject);
			break;
		case "BottomBorder":
			Global.LoseLives (1);
			Instantiate (gameObject, respawnPoint.transform.position, respawnPoint.transform.rotation);
			Destroy (gameObject);	
			break;
		case "WinBox":
			Destroy (col.gameObject);
			Cursor.visible = true; 
			Initialize();
			SceneManager.LoadScene (currentScene.buildIndex + 2);
			break;
		default:
			break;
		}

		if (col.gameObject.tag == "Player") {
			if (playerHit) {
				gameObject.GetComponent <Rigidbody> ().AddForce (gameObject.transform.right * 100f);
				playerHit = false;
			}

			gameObject.GetComponent<Rigidbody> ().AddForce (gameObject.transform.up * 600f);
		}
	}


	// Define what the current scene is
	void Initialize ()
	{
		currentScene = SceneManager.GetActiveScene ();
	}

}
