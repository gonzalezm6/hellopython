import pygame, sys
import qhue
from pygame.locals import *
from qhue import Bridge
from transitions import Machine

b = Bridge("192.168.86.21", "nUS6rndMcOE0XIWIVsxMA8EA-0xJH4Yqk1A-hCDf")
pygame.init()
ds = pygame.display.set_mode((300, 300))
pygame.display.set_caption('Huelights')

blackColor = (0, 0, 0)
greenColor = (0, 200, 0)
ds.fill(blackColor)

HUE = 0

pygame.draw.circle(ds, greenColor, (150, 150), 70)

class Huelights():

	states = ['off', 'onAndGreen', 'red', 'yellow', 'blue', 'purple', 'green']
	
	def _init_(self, name):
		self.machine = Machine(model = self, states = huelights.states, initial = 'off')
		print ("The light is off")
		
		self.machine.add_transition('turnOn', 'off', 'onAndGreen')
		self.machine.add_transition('TurnYellow', 'OnAndGreen', 'yellow')
		self.machine.add_transition('TurnBlue', 'yellow', 'blue')
		self.machine.add_transition('TurnPurple', 'blue', 'purple')
		self.machine.add_transition('TurnRed', '*', 'red')
		self.machine.add_transition('TurnGreen', 'red', 'green')
	
	
	def turnGreen(self):
		print("you turned the light green")
		green = True
		b.lights[1].state(on=True, hue=25500)		
	def turnBlue(self):
		print("you turned the light blue")
		b.lights[1].state(hue=46920)		
	def turnRed(self):
		print("you turned the light red")
		b.lights[1].state(hue=0)
	def increaseHue(self):
		print("you are increasing the hue")
		b.lights[1].state(hue = HUE)
	def decreaseHue(self):
		print("you are decreasing the hue")
		b.lights[1].state(hue = HUE)
	def turnOff (self):
		print("the light is off")
		b.lights[1].state(on = False)
	def turnOn (self):
		print("the light is on and back to green")
		b.lights[1].state(on = True, hue = 25500)		


huelight = Huelights()
   
while True: 
	for event in pygame.event.get():
		if event.type == QUIT:
			pygame.quit()
			sys.exit()
			
		elif event.type == pygame.MOUSEBUTTONDOWN:	
			b.lights[1].state(on=False)	
		elif event.type == pygame.MOUSEBUTTONUP:
			b.lights[1].state(on=True)
		
	pygame.display.update()
	
	
	key = pygame.key.get_pressed()
	if key[K_0]:
		Huelights.turnOff('turnOff')
	if key[K_1]:
		Huelights.turnOn('turnOn')
	if key[K_SPACE]:
		Huelights.turnRed('red')
	if key[K_b]:
		Huelights.turnBlue('blue')
	if key[K_UP]:
		Huelights.increaseHue('increaseHue')
		HUE += 100
	if key[K_DOWN]:
		Huelights.decreaseHue('decreaseHue')
		HUE -= 100
		
