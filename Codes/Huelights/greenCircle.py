import pygame
import qhue
from pygame.locals import *
from qhue import Bridge

pygame.init()
ds = pygame.display.set_mode((300, 300))
pygame.display.set_caption('Huelights')

blackColor = (0, 0, 0)
greenColor = (0, 200, 0)
ds.fill(blackColor)

pygame.draw.circle(ds, greenColor, (150, 150), 70)

while True: 
	for event in pygame.event.get():
		if event.type == QUIT:
			pygame.quit()
			sys.exit()
	pygame.display.update()