import pygame, sys
from pygame.locals import *

pygame.init()
ds = pygame.display.set_mode((600, 600))
pygame.display.set_caption('Robot Face')

mouthx = 1
eyey = 1
nosex = 1

blackColor = (0, 0, 0)
greyColor = (159, 158, 206)
blueColor = (0, 0, 255)
purpleColor = (150, 0, 150)
redColor = (255, 0, 0)
ds.fill(blackColor)

class RobotFace:
	  
	def setState(self, stateStr):
		self.state = stateStr 
		self.movingMouth = False 
	  
	def setMouthMovement(self, mouthBool):
		self.movingMouth = mouthBool
			 
	def drawFace(self):
	  
		#draw head
		head = pygame.draw.rect(ds, greyColor, (30, 30, 300, 300))

		#draw left eye
		leftEye = pygame.draw.circle(ds, blueColor, (100, 125 + eyey), 50)

		#draw right eye
		rightEye = pygame.draw.circle(ds, blueColor, (250, 125 + eyey), 50)

		#draw mouth
		mouth = pygame.draw.rect(ds, purpleColor, (75 + mouthx, 250, 200, 50))
		
		#draw nose
		nose = pygame.draw.rect(ds, redColor, (150 + nosex, 175, 50, 50))
		
robotFace = RobotFace()		


while True: 
  
  robotFace.drawFace()
  
  for event in pygame.event.get():
    if event.type == QUIT:
      pygame.quit()
      sys.exit()
    elif event.type == pygame.MOUSEBUTTONDOWN:
      robotFace.setMouthMovement(True)
      print("mouse down!")
    elif event.type == pygame.MOUSEBUTTONUP:
      robotFace.setMouthMovement(False) 
      print("mouse up!") 
	  
  key = pygame.key.get_pressed()
  if key[K_a]:  
    mouthx -= 2 
  if key[K_d]: 
    mouthx += 2
  if key[K_w]:
    eyey -= 2
  if key[K_s]:
    eyey += 2
  if key[K_LEFT]:
    nosex -= 2
  if key[K_RIGHT]:
    nosex += 2  
	  
      
  pygame.display.update()