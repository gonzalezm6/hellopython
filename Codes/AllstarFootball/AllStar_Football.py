import pygame, sys
from pygame.locals import *
from transitions import Machine
 
pygame.init()

size = [700, 500]
screen = pygame.display.set_mode(size)
 
pygame.display.set_caption("All-star Football")

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

footballPos = 500
footballX = 1
footballY = 1

aimXPos = 350
aimYPos = 150

font = pygame.font.Font(None, 48)
font2 = pygame.font.Font(None, 78)
 
display_instructions = True
instruction_page = 1
 
# -------- Instruction Page Loop -----------
class Player(object):

    states = ['position', 'aim', 'kick']

    def __init__(self, name):
        self.machine = Machine(model = self, states = AllStar_Football.states, initial = 'movePlayer')
        print ("You are moving the player")		

        self.machine.add_transition('movePlayer', '*', 'aimBall')
        self.machine.add_transition('aimBall', '*', 'kickBall')
        self.machine.add_transition('kickBall', '*', 'movePlayer')
		
    def movePlayer (self):
        print("you are positioning the ball")
    def aimBall	(self):
        print("you are aiming the ball")	
    def kickBall (self):	
        print("you kicked to ball")	

while True: 

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            instruction_page += 1
            if instruction_page == 2:
                display_instructions = False
            elif instruction_page >= 2:
                instruction_page = 2
 
    screen.fill(BLACK)
 
    if instruction_page == 1:
 
        text = font.render("All-star Football", True, WHITE)
        screen.blit(text, [220, 215])
 
        text = font.render("Click to play", True, RED)
        screen.blit(text, [250, 260])
		
    if instruction_page == 2:
	
        footballImg = pygame.image.load("football.png")
        footballImg2 = pygame.image.load("smallFootball.png")
        goalPostImg = pygame.image.load("goalPost.png")
        screen.blit(footballImg, (footballPos + footballX, 350 + footballY))
        screen.blit(goalPostImg, (200, 20))
		
        pygame.draw.line(screen, WHITE, (aimXPos, aimYPos), (footballPos + footballX, 350 + footballY))		

        key = pygame.key.get_pressed()
        if key[K_LEFT] and key[K_p]:
            Player.movePlayer('movePlayer')
            footballX -= 2
            if footballPos <= 0 - footballX:           #wrap around screen
                print("too far left")
                footballX = 75			
        if key[K_RIGHT] and key[K_p]:
            Player.movePlayer('movePlayer')
            footballX += 2	
            if footballPos >= 700 - (5/2)*footballX:   #wrap around screen
                print("too far right")
                footballX = -500	
        
        if key[K_LEFT] and key[K_a]:
            Player.aimBall('aimBall')
            aimXPos -= 1
        if key[K_RIGHT] and key[K_a]:
            Player.aimBall('aimBall')
            aimXPos += 1

        if key[K_s]:
            if (aimXPos >= 270 and aimXPos <= 415):
                print("It's good!")
                Player.kickBall('kickBall')
                screen.blit(footballImg2, (aimXPos - 10, aimYPos))								
                text = font2.render("It's good!", True, WHITE)
                screen.blit(text, [230, 85])
            else:
                print("You missed!")
                Player.kickBall('kickBall')
                screen.blit(footballImg2, (aimXPos - 10, aimYPos))				
                text = font2.render("You missed!", True, WHITE)
                screen.blit(text, [200, 85])
        			

		
    if instruction_page >= 2:
        instruction_page += 0	

    pygame.display.flip()
